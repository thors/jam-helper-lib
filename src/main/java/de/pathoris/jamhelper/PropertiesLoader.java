import java.io.File;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Map;

class PropertiesLoader {

  static Properties load(String filePath, Map<String, String> env) throws IOException, SecurityException {
    File configFile = new File(filePath);
    if (!configFile.exists()) {
      //throw new FileNotFoundException(String.format('Property file does not exist: %s', filePath))
      return null;
    }

    return load(configFile, env);
  }

  static Properties load(File configFile, Map<String, String> env) throws IOException, SecurityException{
    Properties properties = new Properties();
    BufferedReader br = new BufferedReader(new InputStreamReader(
    	new FileInputStream(configFile), "UTF-8"));
    properties.load(br);

    for (Object propKey : properties.keySet()) { 
		String value = properties.getProperty(propKey.toString());
		for (String envKey : env.keySet()) {
			value = value.replaceAll("\\$" + envKey, env.get(envKey)); 
		}
		properties.put(propKey.toString(), value);
    }
    return properties;
  }
}
