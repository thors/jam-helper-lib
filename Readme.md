# Jam and Jam-helper-lib

Jam (Jenkins and Monitoring) is a project to manage several
related Jenkins configurations, influx- and postgres-db to collect
statistics and support monitoring, etc. 
The configuration of Jenkins works largely through
init-scripts in init.groovy.d, which read property-files to 
preconfigure different plugins and main-configuration in Jenkins.

Unfortunately, Groovy does not support modularization very well. Reoccuring tasks,
like reading property-files and substitute environment-variables, could 
be implemented in a separate groovy-file and then loaded using the 
GroovyClassLoader, but this looks a bit messy compared to using 
import.

The jam-helper-lib is a small project to support the jam-project. It
started by only providing a PropertiesLoader for the afore-mentioned task.
It is very likely that it still didn't get additional features.

However, coincidentally I will use this library to get more familiar 
with the gitlab ci/cd process, with gradle, etc.

